SparkCore HT1632C
=================

Spark Core library for HT1632C based LED dot matrix displays (eg. Sure Electronics).

Based on the [Arduino HT1632C library](https://code.google.com/p/ht1632c/) by *unknown*.

![Demo](http://tinkerman.eldiariblau.net/wp-content/uploads/2014/02/ht1632c_example.jpg)

This code is in ALPHA stage. Currently supports:

* drawing graphic primitives (points, lines, circles,...)
* drawing single color bitmaps
* printing chars and texts in fixed positions (no scrolling)
* RG colors (red, green and orange)
* 23 different fonts
* 16 brightness levels
* Horizontal and vertical scroll

Tested only on 32x16 displays, other geometries might not work.

## TODO:

* Documentation
* Tricolor characters
* Blinking and fading effects

## Building

Copy the contents of `src` and `inc` folders to the core-firmware folder and build the example code (`src/application.cpp`).
If you want to add the library to your project, copy the files src/ht1632c.cpp, inc/ht1632c.h and inc/font.h to your project
and change your build.mk to instruct the compiler to include the file src/ht1632c.cpp file.
