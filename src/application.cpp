#include "application.h"
#include "ht1632c.h"

#define MATRIX_DATA_PIN     D6 // green
#define MATRIX_CLK_PIN      D3 // white
#define MATRIX_CS_PIN       D4 // brown
#define MATRIX_WR_PIN       D5 // yellow

#define BRIGHTNESS          10

ht1632c matrix = ht1632c(MATRIX_DATA_PIN, MATRIX_WR_PIN, MATRIX_CLK_PIN, MATRIX_CS_PIN, 2);

static const uint16_t smile[8] = { 0x3C, 0x42, 0xA5, 0x81, 0xA5, 0x99, 0x42, 0x3C };

void sendText() {
    matrix.hScroll(6, "LONG", GREEN, SCROLL_LEFT | SCROLL_PAUSE, 20);
}

boolean paused = false;

void setup() {

    matrix.begin();
    matrix.setBrightness(BRIGHTNESS);

    /*
    matrix.circle(32, 7, 6, GREEN);
    matrix.fill(5, 5, RED);
    matrix.bezier(1, 1, 4, 12, 31, 15, ORANGE);
    matrix.ellipse(14, 4, 24, 12, ORANGE);
    */

    /*
    matrix.setFont(FONT_5x7);
    matrix.putBitmap(3, 4, (uint16_t *) smile, 8, 8, RED);
    matrix.putText(14, 2, "SPARKCORE", GREEN);
    matrix.putText(24, 9, "ROCKS", RED);
    matrix.rect(0, 0, 63, 15, ORANGE);
    matrix.sendframe();
    */

    matrix.setFont(FONT_8x8);
    sendText();

}

void loop() {
    matrix.scroll();
    if (!matrix.scrolling()) sendText();
}
